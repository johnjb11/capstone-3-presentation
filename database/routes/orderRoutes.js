const express = require("express");
const router = express.Router();
const orderController = require('../controllers/orderControllers');
const auth =  require('../auth')

//create Order
router.post('/order', auth.verify, (req, res) => {
    const userData =  auth.decode(req.headers.authorization)

  if(userData.isAdmin === false){
    orderController.createOrder(userData, req.params, req.body).then(resultFromController=> res.send(resultFromController))
  } else {
    res.send("Authorized users only.")
  }
})

//retrieve order
router.get("/myOrder", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);

  orderController.getOrder(userData).then(resultFromController => res.send(resultFromController));
})

//retrieve all orders admin

router.get("/allOrder" , auth.verify, (req, res) => {
const userData = auth.decode(req.headers.authorization);

  if(userData.isAdmin === true){
  orderController.getAllOrders().then(resultFromController => res.send(resultFromController))
  } else {
    return res.send('Authorized users only.')
  }
});

//update quantity
router.put('/:orderId/update', auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);

  orderController.updateOrder(userData, req.params, req.body).then(resultFromController => res.send(resultFromController))
})

//delete order
router.delete('/:orderId/delete' , auth.verify, (req, res)=> {
  const userData = auth.decode(req.headers.authorization);

  if(userData.isAdmin === false){
    orderController.deleteOrder(req.params).then(resultFromController => res.send(resultFromController));
  }else {
    return res.send('Authorized users only.')
  }
})

module.exports = router;

//archive order 
router.put('/:orderId/archive', auth.verify, (req,res) => {
  const userData = auth.decode(req.headers.authorization);

  if(userData.isAdmin === false){
    orderController.archiveOrder(req.params).then(resultFromController => res.send(resultFromController))
  } else {
    return res.send('Authorized users only.')
  }
})

//remove item cart
router.delete('/:orderId/deleteItem', auth.verify, (req, res)=> {
  const userData = auth.decode(req.headers.authorization);

  if(userData.isAdmin === false){
    orderController.removeCartItem(userData, req.params, req.body).then(resultFromController => res.send(resultFromController));
  }else {
    return res.send('Authorized users only.')
  }  
})

//Edit Cart item Qty
router.put('/:orderId/edit', auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);

  console.log(userData)
   if(userData.isAdmin === false){
    orderController.editCartItem(userData, req.params, req.body).then(resultFromController => res.send(resultFromController));
  }else {
    return res.send('Authorized users only.')
  }  
})
