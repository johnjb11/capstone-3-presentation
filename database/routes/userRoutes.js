const express = require("express");
const router = express.Router();
const auth = require('../auth')
const userController = require('../controllers/userControllers')

router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
});

router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});

router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
});


//make as admin
router.put('/:userId/setAsAdmin', auth.verify,(req,res) => {

	const userData = auth.decode(req.headers.authorization);

	if (userData.isAdmin === false) {
		userController.makeAsAdmin(req.params,req.body).then(resultFromController =>res.send("Changed status to ADMIN."))
	} 
	else {
		res.send("Only admin users can change admin functionality.")
	}

})

router.get("/details", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
		userController.getProfile({userId : userData.id}).then(resultFromController => res.send(resultFromController));
});


router.get("/myOrderHistory", auth.verify, (req, res)=> {
	const userData = auth.decode(req.headers.authorization)

	userController.getOrderHistory(userData.id).then(resultFromController => res.send(resultFromController))
})


module.exports = router;

