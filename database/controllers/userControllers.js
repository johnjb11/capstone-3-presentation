const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");
const Product = require('../models/Product')
const Order = require('../models/Order')



// checking if email exists
module.exports.checkEmailExists = (reqBody) => {
	return User.find({email : reqBody.email}).then(result => {
		if (result.length > 0) {
			return true;

		} else {

			return false;

		};
	});

};

// user registration

module.exports.registerUser = (reqBody) => {

			let newUser = new User({
				firstName : reqBody.firstName,
				lastName : reqBody.lastName,
				address:reqBody.address,
				email : reqBody.email,
				mobileNo : reqBody.mobileNo,
				password : bcrypt.hashSync(reqBody.password, 10)
			})

			return	newUser.save().then((user, error) => {
		if(error) {
			return "Unsuccessful. Check your credentials again."
		} else {
			return "You have successfully created your account."

		};

	});

};



// login route

module.exports.loginUser = (reqBody) => {
		
	return User.findOne({email : reqBody.email}).then(result => {

		if(result == null){

			return "Cannot find user."

		} else {

		const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if (isPasswordCorrect) {

				return { access : auth.createAccessToken(result) }

			} else {

				return "Wrong password."

			};

		};

	});

};

module.exports.makeAsAdmin = (reqParams, reqBody) => {
	let changedToAdmin = {
		isAdmin : true
	}

	return User.findByIdAndUpdate(reqParams.userId, changedToAdmin).then((user,error) => {
		if(error) {
			return "Cannot change admin functionality."
		} else {
			return "Changed status of user as admin."
		}
	})
}

module.exports.getAllUsers = () => {

	return User.find({}).then(result => {
		return result;
	})
};

// get details

module.exports.getProfile = (data) => {
console.log(data)
			return User.findById(data.userId).then(result => {

				
				result.password = "";

				
				return result;

			});

		};


//retrieve user order
module.exports.getOrder = (userId,reqBody) => {

	return User.findById(userId).then(result =>{

		let orderSpecific = {
			id: result.id,
			orders: result.orders} 

		return result;
		
	})
}

//retrieve orders admin

module.exports.getOrderHistory = (userId) => {
	return Order.find({userId: userId, status: false}).populate({path: 'cartList', populate: 'productId'}).then(result => {
		return result
	})
}


//retrieve all orders

module.exports.getAllOrders = () =>{

	return User.find({}).then(result =>{
		return result;
	})
};


