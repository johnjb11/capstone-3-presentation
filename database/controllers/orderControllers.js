const Order = require('../models/Order')
const User = require('../models/User');
const Product = require('../models/Product');
const bcrypt = require('bcrypt');
const auth = require('../auth');

module.exports.createOrder = async (userData, reqParams, reqBody) =>{

    let {orderId, productId, quantity, userId, totalAmount, status, subTotal, cartList} = reqBody

    quantity = Number.parseInt(reqBody.quantity)

    try{
        let order = await Order.findOne({userId: userData.id, status: true});
    
        const productDetails = await Product.findById(productId)


        if(order && order.status === true){
    
            let cartListIndex = order.cartList.findIndex(p => p.productId == productId);
     
            if (cartListIndex > -1){
                let orderItem = order.cartList[cartListIndex];
                orderItem.quantity += quantity
                orderItem.subTotal = orderItem.quantity * productDetails.price
                order.totalAmount = order.cartList.reduce((a, {subTotal})=> a + subTotal,0)
            
                order.cartList[cartListIndex] = orderItem;
       
                 productDetails.inStock -= 1

                 return await order.save().then(() => {
                    return order
                 }).catch((err) => {
                    return err
                 })

            } else {
                 order.cartList.push({productId, quantity, subTotal: parseInt(productDetails.price * quantity)});
                 order.totalAmount = order.cartList.reduce((a, {subTotal})=> a + subTotal,0)
                 return await order.save().then(() => {
                    return order
                 }).catch((err) => {
                    return err
                 })
            }

            order = await order.save().then(()=> {
                return order; 
            }).catch((err)=> {
                return err
            })
        } else {

            const newOrder = await new Order({
                userId,
                cartList: [{productId, quantity, subTotal: parseInt(productDetails.price * quantity) }],
                totalAmount: productDetails.price
                
            });
            console.log(totalAmount)
            console.log(newOrder)
            return await newOrder.save();
        }
    } catch (err) {
        console.log(err)
        return err
    }
}

// retrieve order
module.exports.getOrder = (userData) => {

    return Order.find({userId: userData.id, status: true}).populate({path: 'cartList', populate: 'productId'}).then(result => {

        return result
        console.log(result)
    }).catch(() => {
        return false
    })
}

//retrieve all orders admin
module.exports.getAllOrders = () => {
    return Order.find({status: false}).populate({path: 'cartList', populate: 'productId'}).sort({purchasedOn: -1}).then(result => {
        return result;
    })
}
 

//delete order
module.exports.deleteOrder = (reqParams) => {

    return Order.findByIdAndDelete(reqParams.orderId).then(result => {
        console.log(result)
        return  result.save().then(()=> {
            return result
        })
    }).catch((err) => {
        return err
    })
}

//archive order
module.exports.archiveOrder = (reqParams) => {

    return Order.findByIdAndUpdate(reqParams.orderId).then(result => {
        if(result.status === true){

            result.status = false
            return result.save().then(()=> {
                return result
            }).catch(()=> {
                return false
            }) 
        } else { 
            return result
        }
    })

}

//remove item cart

module.exports.removeCartItem = async (userData, reqParams, reqBody) => {

    let {productId, totalAmount, status, subTotal, cartList} = reqBody

    let {orderId} = reqParams

    try{

        let order = await Order.findOne({userId: userData.id, status: true});

        if(order && order.status === true){

            let cartListIndex = order.cartList.findIndex(p => p.productId == productId);

     

            if(cartListIndex > -1){

                order.cartList.splice(cartListIndex, 1)
                order.totalAmount = order.cartList.reduce((a, {subTotal})=> a + subTotal,0)
      
                return await order.save().then(() => {
            
                    return order
                }).catch((err) => {
                    return err
                })

            }
        } else {
            return false
        } 
        
    }catch (err) {
        console.log(err)
        return err
    } 
}

// edit cart

module.exports.editCartItem = async (userData, reqParams, reqBody) => {
    
    let {productId, totalAmount, status, subTotal, cartList, quantity} = reqBody
    console.log(reqBody)
    let {orderId} = reqParams

    try{
        let order = await Order.findOne({userId: userData.id, status: true});

        const productDetails = await Product.findById(productId)
      
        if(order && order.status === true){

            let cartListIndex = order.cartList.findIndex(p => p.productId == productId);
           
            if(cartListIndex > -1){

                let orderItem = order.cartList[cartListIndex];
                console.log(reqBody.quantity)
                orderItem.quantity = quantity
                orderItem.subTotal = orderItem.quantity * productDetails.price
                order.totalAmount = order.cartList.reduce((a, {subTotal})=> a + subTotal,0) 

                order.cartList[cartListIndex] = orderItem;
            
                 return await order.save().then(() => {
                
                    return order
                 }).catch((err) => {
                    return err
                 })

            } else {
                return false
            }
        } else {
            return false
        }

    }catch (err) {
        console.log(err)
        return err
}
}

